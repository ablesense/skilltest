# Developer applicant test

A simple responsive one-pager used to assess a new applicant's skill level


## Objectives

Make a simple one-page website based on the designs found in ```source/design``` using modern web standards. 
The result has to work in modern browsers like Mozilla Firefox, Google Chrome and safari, and look good on a variety of different screen sizes and devices.

- Complete the index.html file
- Use LESS or straight up CSS to style the page
- Write JavaScript and CSS to transform the navigation on mobile devices based on the design
- The gallery starts of with 3 images on mobile, use JavaScript to load extra gallery images depending on the screen size (e.g.: 3 for mobile, 6 for tablets, 9 for desktop)


## Resources

All resources are available in the ```source``` folder. Be it in their most raw form or ready to be used directly. 
It's up to you to prepare/optimize what is not ready for the web.


## Priorities

- We expect a completed index.html file with nothing but valid markup.
- CSS for all breakpoints. Don't spend too much time on the minutiae of the design, in stead make sure that every part of the page has at least some styling.
- A working collapsable mobile navigation.


## Time

In case you are about to run out of time, take about 5 minutes to at least *think* about a solution for every problem, even write some commented out pseudo-code.

