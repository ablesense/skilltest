/*
Author: Able Sense Media
Web: ablesense.com
Date of creation: 2014/01/01
*/

var APP = (function () {
	var me = {},
		browser = {}

	/////////////////////////////////////////////////////////////////
	////////////////////// PRIVATE FUNCTIONS ////////////////////////
	/////////////////////////////////////////////////////////////////
		//private vars
		;

	function getSVGsupport() {
		return document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#BasicStructure", "1.1");
	}

	function getViewportSize() {
		browser.viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
		browser.viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
	}

	function debounce(func, wait, immediate) {
		var timeout;
		return function() {
			var context = this, args = arguments;
			clearTimeout(timeout);
			timeout = setTimeout(function() {
				timeout = null;
				if (!immediate) func.apply(context, args);
			}, wait);
			if (immediate && !timeout) func.apply(context, args);
		};
	}

	/////////////////////////////////////////////////////////////////
	////////////////////// PUBLIC FUNCTIONS /////////////////////////
	/////////////////////////////////////////////////////////////////

	me.cancelEvent = function(event) {
		if (event.preventDefault) {
			event.preventDefault();
		} else {
			event.returnValue = false;
		}
	};

	me.onResize = function(callback) {
		callback();

		$(window).on('resize', debounce(function() {
			callback();
		}, 200));
	};


	browser.supportsSVG = getSVGsupport();
	browser.viewportSize = getViewportSize();
	browser.viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
	browser.viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

	me.onResize(getViewportSize);

	me.browser = browser;

	return me;
})();

(function(){
	document.documentElement.classList.remove('no-js');

	if (!APP.browser.supportsSVG) {
		document.documentElement.classList.add('no-svg');
	}

	var images = [
		{
			"title": "Breath taking mountains sky high nature",
			"src": "/img/gallery/breath-taking-mountains-sky-high-nature.jpg"
		},
		{
			"title": "Mountain lake view paul e harrer nature",
			"src": "/img/gallery/mountain-lake-view-paul-e-harrer-nature.jpg"
		},
		{
			"title": "Dark sunset snow mountain nature",
			"src": "/img/gallery/dark-sunset-snow-mountain-nature.jpg"
		},
		{
			"title": "Mountain blur minimal nature",
			"src": "/img/gallery/mountain-blur-minimal-nature.jpg"
		},
		{
			"title": "Mountain high sky nature rocky",
			"src": "/img/gallery/mountain-high-sky-nature-rocky.jpg"
		},
		{
			"title": "Winter mountain woods tree nature cold",
			"src": "/img/gallery/winter-mountain-woods-tree-nature-cold.jpg"
		}
	];





})();